from gym import Env
from gym.spaces import Discrete, Box

from stable_baselines3 import PPO

import numpy as np

class agent_drl(Env):
    def __init__(self, gridworld):
        self.gridworld = gridworld
        
        self.action_space = Discrete(4)
        self.observation_space = Box(low = 0, high = 1, shape = (self.gridworld.n_rows,
                                                                 self.gridworld.n_cols))

        self.agent = PPO("MlpPolicy", self, device="cpu", verbose = 0)

    def reset(self):
        pos = self.gridworld.reset()
        state = np.zeros(shape = self.observation_space.shape)
        state[pos[0]][pos[1]] = 1
        return state

    def step(self, action):
        g_action = self.action_to_string(action)
        pos, reward, terminal, info = self.gridworld.step(g_action)

        state = np.zeros(shape = self.observation_space.shape)
        state[pos[0]][pos[1]] = 1

        return state, reward, terminal, info

    def action_to_string(self, action):
        if action == 0:
            return "up"
        if action == 1:
            return "left"
        if action == 2:
            return "right"
        if action == 3:
            return "down"
        
    def run_episode(self):
        state = self.reset()
        terminal = False
        while not terminal:
            print(self.gridworld.pos)
            a, _ = self.agent.predict(state, deterministic = True)
            print(self.action_to_string(a))
            state, _, terminal, _ = self.step(a)

    def train_agent(self, steps = 10000):
        self.agent.learn(steps)

g = gridworld(5, 5)
g.grid[0][2] = 4
a_drl = agent_drl(g)
a_drl.train_agent()
a_drl.run_episode()

# a_drl.train_agent()
# a_drl.run_episode()
