import random

class agent():
    def __init__(self, gridworld):
        self.gridworld = gridworld

        # S->A->U
        self.q = {}
        self.alpha = 0.1
        self.gamma = 0.9
        self.epsilon = 0.1

        self.reset()

    def reset(self):
        self.terminal = False
        self.reward = 0
        self.state = copy.copy(self.gridworld.reset())

    def step(self, debug = False, action = None):
        if debug: print("State:", self.state)
        if not action:
            self.action = self.choose_action(self.state, debug)
        else:
            self.action = action
            
        if debug:
            print("Action:", self.action)
        next_state, self.reward, self.terminal, _ = self.gridworld.step(self.action)
        if debug:
            print("Reward:", self.reward)
            print("Next state:", next_state)
        # Learning
        if debug:
            if repr(self.state) in self.q: 
                print("Q before:", self.q[repr(self.state)])
            else:
                print("No Q in this state yet")
        self.learn_q(next_state, self.reward)
        if debug:
            print("Q after:", self.q[repr(self.state)])
        self.state = copy.copy(next_state)
        if self.terminal:
            if debug: print("Terminal state")


    def run_episode(self, debug = False, terminal_at = 100):
        self.reset()
        i = 0
        total_reward = 0
        while (not self.terminal) and (i < terminal_at):
            self.step(debug)
            i += 1
            total_reward += self.reward
        return i, total_reward

    # Epsilon greedy action selection (policy) (cf. softmax)
    def choose_action(self, state, debug = False):
        # Explore?
        if random.random() < self.epsilon:
            if debug: print("Explore")
            return random.choice(self.gridworld.action_space)
        # Exploit
        if debug: print("Exploit")
        return self.max_q(state)[1]

    def learn_q(self, next_state, reward):
        if repr(self.state) not in self.q:
            self.q[repr(self.state)] = {}
            for a in self.gridworld.action_space:
                self.q[repr(self.state)][a] = 0.0

        previous_q = self.q[repr(self.state)][self.action]
        new_q = previous_q + self.alpha*(reward + self.gamma*self.max_q(next_state)[0] - previous_q)
        self.q[repr(self.state)][self.action] = new_q

    def max_q(self, state):
        if repr(state) not in self.q:
            return 0, random.choice(self.gridworld.action_space)
        max_q = None
        best_action = None
        for a in self.gridworld.action_space:
            if not max_q or self.q[repr(state)][a] > max_q:
                max_q = self.q[repr(state)][a]
                best_action = a
        return max_q, best_action

g = gridworld(5, 5)
a = agent(g)

# Train agent
for e in range(10000):
    i, r = a.run_episode()
    #print(e, i, r)

# Test agent
a.epsilon = 0
print(a.run_episode())

g.grid[0][2] = 4
a.epsilon = 0.1
for e in range(10000):
    i, r = a.run_episode()
    #print(e, i, r)

a.epsilon = 0
print(a.run_episode())
