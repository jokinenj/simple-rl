import copy

class gridworld():
    def __init__(self, n_rows, n_cols):
        self.n_rows = n_rows
        self.n_cols = n_cols

        self.grid = [[0, 0, 0, 0, 0],
                     [0, 0, 2, 0, 0],
                     [0, 0, 2, 3, 0],
                     [0, 0, 2, 0, 0],
                     [0, 0, 0, 0, 0]]

        self.starting_pos = [1, 0]

        self.action_space = ["up", "left", "down", "right"]

    def reset(self):
        self.pos = copy.copy(self.starting_pos)
        return self.pos

    def step(self, action):
        previous_pos = copy.copy(self.pos)
        if action == "up":
            self.pos[0] = max(self.pos[0] - 1, 0)
        if action == "down":
            self.pos[0] = min(self.pos[0] + 1, self.n_rows-1)
        if action == "left":
            self.pos[1] = max(self.pos[1] - 1, 0)
        if action == "right":
            self.pos[1] = min(self.pos[1] + 1, self.n_cols-1)

        if self.grid[self.pos[0]][self.pos[1]] == 2:
            self.pos = previous_pos


        self.reward = -0.1
        self.terminal = False

        if self.grid[self.pos[0]][self.pos[1]] == 4:
            self.reward = -10
        
        if self.grid[self.pos[0]][self.pos[1]] == 3:
            self.reward = 10
            self.terminal = True

        return self.pos, self.reward, self.terminal, {}
            
            
        
                

